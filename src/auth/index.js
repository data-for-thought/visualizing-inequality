import auth0 from 'auth0-js'

const webAuth = new auth0.WebAuth({
  domain: 'gatherer.auth0.com',
  clientID: '7Axo4oVOQQDIHDmmBO7JLh2I5lGaErSN',
  // make sure port is 8080
  redirectUri: 'http://localhost:8080/callback',
  // we will use the api/v2/ to access the user information as payload
  audience: 'https://gatherer.auth0.com/api/v2/',
  responseType: 'token id_token',
  scope: 'openid profile'
})

export default {
  webAuth
}
